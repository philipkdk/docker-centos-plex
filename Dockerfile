FROM ninstaah/docker-centos
MAINTAINER ninstaah <hello@ninstaah>

# Fix broken link for /var/lock
RUN mkdir -p /run/lock/subsys

RUN yum -y update; yum clean all
RUN yum -y install initscripts; yum clean all
RUN rpm -Uvh https://downloads.plex.tv/plex-media-server/1.0.0.2261-a17e99e/plexmediaserver-1.0.0.2261-a17e99e.x86_64.rpm; echo 'avoiding error'
RUN mkdir /config && \
    mkdir /data && \
    chown plex:plex /config && \
    chown plex:plex /data 
VOLUME ["/config"]
VOLUME ["/data"]

ADD PlexMediaServer /etc/sysconfig/PlexMediaServer
ADD start.sh /start.sh

EXPOSE 32400

CMD ["/bin/bash", "/start.sh"]